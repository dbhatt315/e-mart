import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ContextProvider } from './Components/Context';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <ContextProvider>  
    <App />
  </ContextProvider>
,document.getElementById('root')
);
reportWebVitals();

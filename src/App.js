import './App.css';
import Navbar from './Components/Navbar';
import {BrowserRouter as Router,Routes, Route} from 'react-router-dom';
import Home from './Components/pages/Home';
import Catalog from './Components/pages/Catalog';
import CatalogSingleProduct from './Components/pages/CatalogSingleProduct';
import SignIn from './Components/pages/SignIn';
import SignUp from './Components/pages/SignUp';
import Cart from './Components/pages/Cart';
import React, {Component } from 'react';
import Checkout from './Components/pages/Checkout';
import Analytics from './Components/pages/Analytics';
import {useState} from 'react';
function App() {
  var initState = {
    cart: [],
    user : {
      email: null,
      token: null,
      tokenDate: null,
      role: null,
      authenticated: false,
    },
    user_info:{
      email: null,
      firstName: null,
      lastName: null,
      addressBill: null,
      provinceBill: null,
      countryBill: null,
      zipBill: null,
      addressShip: null,
      provinceShip: null,
      countryShip: null,
      zipShip: null,
    },
    purchase_id:null,
    redirect_left:'',
    user_cart:{
      subtotal: 0,
      total:0,
    }
  };


  const [state, setState] = useState(initState);
  
  sessionStorage.setItem('userState',JSON.stringify(state));
  
  return (
   <>
   <Router>
      <Navbar />
      <Routes>
        <Route path='/' exact element={<Home/>}/>
        <Route path='/browse' exact element={<Catalog/>}/>
        <Route path='/browse/product' exact element={<CatalogSingleProduct/>}/>
        <Route path='/sign-in' exact element={<SignIn/>}/>
        <Route path='/sign-up' exact element={<SignUp/>}/>
        <Route path='/shopping_cart' exact element={<Cart/>}/>
        <Route path='/browse/product/checkout' exact element={<Checkout/>}/>
        <Route path='/analytics' exact element={<Analytics/>}/>

      </Routes>
    </Router>
   </>
  );
}

export default App;

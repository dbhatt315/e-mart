import React, {useState} from 'react';
import { Button } from '../Button';
import './SignUp.css';
import {Link, useNavigate} from 'react-router-dom';

const SignUp = () => {
  var userState = JSON.parse(sessionStorage.getItem('userState'));
  const navigate = useNavigate();
  const [errorThrown, setErrorThrown] = useState(false);
  const errorThrownToggle = () => setErrorThrown(!errorThrown);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  const [passwordConf,setPasswordConf] = useState("");
  const [phoneNumber,setPhoneNumber] = useState("");
  const [addressBill,setAddressBill] = useState("");
  const [provinceBill,setProvinceBill] = useState("");
  const [countryBill,setCountryBill] = useState("");
  const [zipBill,setZipBill] = useState("");
  const [addressShip,setAddressShip] = useState("");
  const [provinceShip,setProvinceShip] = useState("");
  const [countryShip,setCountryShip] = useState("");
  const [zipShip,setZipShip] = useState("");
  const URL = 'http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com'
  return (
    <div className='signUp_container'>
        <div className='signUp_wrapper'>
            <h1>Create Account</h1>
            <p className='error-thrown' style={{ display: !errorThrown ? "none" : "block"}}>*Email address provided already exists</p>
            <form>
                <div className='signup-name-container'>
                    <input placeholder='First Name' onChange={(e)=>setFirstName(e.target.value)}required></input>
                    <input placeholder='Last Name' onChange={(e)=>setLastName(e.target.value)}required></input>
                </div>
                <input placeholder='Email' onChange={(e)=>setEmail(e.target.value)} required></input>
                <input placeholder='Password' type='password' onChange={(e)=>setPassword(e.target.value)}required></input>
                <input placeholder='Confirm Password' type='password' onChange={(e)=>setPasswordConf(e.target.value)}required></input>
                <input placeholder='Phone Number' onChange={(e)=>setPhoneNumber(e.target.value)} required></input>
                <h3>Billing Information</h3>
                <input placeholder='Address' onChange={(e)=>setAddressBill(e.target.value)}required></input>
                <input placeholder='Province' onChange={(e)=>setProvinceBill(e.target.value)}required></input>
                <div className='signup-countryzipcode-container'>
                    <input placeholder='Country' onChange={(e)=>setCountryBill(e.target.value)}required></input>
                    <input placeholder='Zip Code' onChange={(e)=>setZipBill(e.target.value)}required></input>
                </div>
                <h3>Shipping Information</h3>
                <input placeholder='Address' onChange={(e)=>setAddressShip(e.target.value)}required></input>
                <input placeholder='Province' onChange={(e)=>setProvinceShip(e.target.value)}required></input>
                <div className='signup-countryzipcode-container'>
                    <input placeholder='Country' onChange={(e)=>setCountryShip(e.target.value)}required></input>
                    <input placeholder='Zip Code' onChange={(e)=>setZipShip(e.target.value)}required></input>
                </div>
                <div className='signUp-btns'>
                    <Button className='btns' buttonStyle='btn--outline'
                    buttonSize='btn--large' type='button' onClick={()=>{
                      
                      if (checkValidationInput(email,password,passwordConf,firstName,lastName,phoneNumber,addressShip,provinceShip,countryShip,
                        zipShip,addressBill,provinceBill,countryBill,zipBill)){
                          
                        fetch(`http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com/user/register?email=${email}&password=${password}&firstname=${firstName}&lastname=${lastName}&phone=${phoneNumber}&streetShip=${addressShip}&provinceShip=${provinceShip}&countryShip=${countryShip}&zipShip=${zipShip}&role=CUSTOMER&streetBill=${addressBill}&provinceBill=${provinceBill}&countryBill=${countryBill}&zipBill=${zipBill}`, {
                              method: 'POST',
                          })
                          .then(response => response.json())
                          .then(json => {
                              
                              if(!json.error){
                                  console.log('User Successfully registered');
                                  const userState = JSON.parse(sessionStorage.getItem('userState'));
                                  userState.email = email;
                                  userState.firstName = firstName;
                                  userState.lastName = lastName;
                                  userState.addressBill = addressBill;
                                  userState.provinceBill = provinceBill;
                                  userState.countryBill = countryBill;
                                  userState.zipBill = zipBill;
                                  userState.addressShip = addressShip;
                                  userState.provinceShip = provinceShip;
                                  userState.countryShip = countryShip;
                                  userState.zipShip = zipShip;
                                  navigate('/sign-in');
                                  setErrorThrown(false);
                              }else{
                                  if (!errorThrown){
                                      setErrorThrown(true);
                                  }
                              };
                          }).catch((error)=> console.log(error));
                      }
                    }}>Sign Up</Button>
                </div>
            </form>
        </div>
    </div>
  )
}

function checkValidationInput(email,password,passwordConf,firstName,lastName,phoneNumber,addressShip,provinceShip,countryShip,
  zipShip,addressBill,provinceBill,countryBill,zipBill){ 
    if (email.length>0 && password.length>0 && passwordConf.length>0 && firstName.length>0 && lastName.length>0 && phoneNumber.length>0
      && addressShip.length>0 && provinceShip.length>0 && countryShip.length>0 && zipShip.length>0 && addressBill.length>0 && provinceBill.length>0
      && countryBill.length>0 && zipBill.length>0){
        return true;
    }else{
      return false;
    }

}

export default SignUp
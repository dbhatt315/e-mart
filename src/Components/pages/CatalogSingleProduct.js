import React, { useEffect, useState, useTransition } from 'react'
import { useLocation } from 'react-router-dom'
import './CatalogSingleProduct.css'

const CatalogSingleProduct = () => {
    const location = useLocation()
    const [item, setItem] = useState({})
    const [qnty, setQnty] = useState(1)
    const URL = 'http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com'

    
    useEffect(() => {
        location.state.qtn = qnty;
        setItem(location.state);
    }, [])

    const sendToCart = () => {
        var userState = JSON.parse(sessionStorage.getItem('userState'));
        if(!userState.cart){
            userState.cart = [item];
        }else{
            const found_elem = userState.cart.find(elem => elem.id==item.id);
            if (found_elem){
                found_elem.qtn+=item.qtn;
            }else{
                userState.cart.push(item);
            }
        }
        sessionStorage.setItem('userState',JSON.stringify(userState));
    }

    const sendReview = () => {
        const userState = JSON.parse(sessionStorage.getItem('userState'));
        if (userState.user == null || userState.user.authenticated != true) {
            alert('You must sign in to leave a review!');
        } else {
            const textval = document.getElementsByClassName('product-review-input')[0].value;
            const numrev = document.getElementById('revRating').value;
            if (textval == undefined || textval.length < 1 || numrev == undefined || numrev == '') {
                alert('You must fill out the review box before submitting and rating must be between 1 and 5.');
            } else {
                console.log(`${URL}/review/newReview?userPostId=${userState.user.email}&rating=${numrev}&reviewDesc=${textval}&itemId=${item.id}`);
                fetch(`${URL}/review/newReview?userPostId=${userState.user.email}&rating=${numrev}&reviewDesc=${textval}&itemId=${item.id}`,{
                    method: 'POST',
                    headers: new Headers({'Authorization' : userState.user.token})
                 }).then(response => response.json()).then(json => { alert(json['response'])
                }).catch((error)=> console.log(error));
           }
        }
        console.log(document.getElementsByClassName('product-review-input')[0].value);
        }
    
      window.onload = function() {
            console.log('Loading');
            console.log(`${URL}/review/getReviewsItem?itemId=${item.id}`);
            fetch(`${URL}/review/getReviewsItem?itemId=${item.id}`, {method: 'GET'}).then(response => response.json()).then(json => { 
                console.log(json);
                let table = "<table style='border: medium; border-color: black'> <tr> <th> User </th> <th> Review </th> <th> Rating </th> </tr>";
                if (json != undefined || json[0]['review_id'] != undefined) {
                    let i = 0;
                    console.log("Look here");
                    for (i=0; i<json.length; i++) {
                        table += "<tr> <td> " + json[i]['userPost_id'] + "</td> <td> " + json[i]['review'] + " </td> <td> " + json[i]['rating'] + " </td> </tr>";
                    }
                }
            
                table += " </table>";
                let tablrev = document.getElementById('ReviewList');
                tablrev.innerHTML = table;
            }).catch((error)=> {console.log(error)});
        }
    
    

    return (
        <div>
            <div className='wrapper'>
                <div className='img-container'>
                    <img className='single-product-img' src={item.link}/>
                </div>
                <div className='info-container'>
                    <h1 className='product-title'>{item.name}</h1>
                    <p className='product-desc'>{item.description}</p>
                    <p className='product-review'>4.5/5{/*item.review*/}</p>
                    <span className='product-price'>${item.price}</span>
                    <div className='product-bt'>
                        <span className='product-brand'><b>Brand:</b> {item.brand}</span>
                        <span className='product-type'><b>Type:</b> {item.type}</span>
                    </div>
                    <div className='add-container'>
                        <div className='amount-container'>
                            <i className="fa-solid fa-minus" cursor='pointer' onClick={() => {
                                var newQnty = (qnty - 1) < 1 ? item.quantity : (qnty-1);
                                setQnty(newQnty);
                                item.qtn = newQnty;
                            }}/>
                            <span className='amount'>{qnty}</span>
                            <i className='fa-solid fa-plus' cursor='pointer' onClick={()=>{
                                var newQnty = (qnty + 1) > item.quantity ? 1: (qnty+1);
                                setQnty(newQnty);
                                item.qtn = newQnty;
                                }}/>
                        </div>
                        <button className='cart' onClick={()=>sendToCart()}>Add to Cart</button>    
                    </div>
                    <div className='review-container'>
                        <form>
                            <legend className='review-title'>Add a review</legend>
                            <textarea className='product-review-input' placeholder='review...'></textarea>
                            {'\n'}
                            <label for="revRating">Quantity (between 1 and 5):</label>
                            <input type="number" id="revRating" name="revRating" min="1" max="5" step="0.1"></input>
                        </form>
                        <button className='review-button' onClick={() => sendReview()}>Submit</button>
                    </div>
                    <div id="ReviewList" ></div>
                </div>
            </div>
        </div>
    )
}

export default CatalogSingleProduct
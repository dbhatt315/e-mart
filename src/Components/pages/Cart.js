import React, {useContext, useEffect, useState} from 'react'
import { SummaryContext } from '../Context';
import {useNavigate} from 'react-router-dom';
import './Cart.css';
import CartProducts from './CartProducts';


const Cart = () => {
    const navigate = useNavigate()
    // var [subTotal, setSubTotal] = useState(0.0);
    const {subtotal, setSubTotal, total, setTotal, taxes, setTaxes, changeInCart,setChangeInCart, displayCartProducts, setDisplayCartProducts} = useContext(SummaryContext);
    const URL = 'http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com'

    useEffect(() => {
        var userObj = JSON.parse(sessionStorage.getItem('userState'));
        for(const item of userObj.cart){
            userObj.user_cart.subtotal += Math.round(((item.price*item.qtn) + Number.EPSILON) * 100) / 100;
        }
        setSubTotal(userObj.user_cart.subtotal)
        setDisplayCartProducts(
            <CartProducts items={userObj.cart}/>
        );
    }, []);

  const readyForCheckout = () => {
      const userState = JSON.parse(sessionStorage.getItem('userState'));
      if(userState.cart.length > 0){
        if(userState.user.authenticated == true){
            console.log("HEDSFSDFDSF")
            fetch(`${URL}/order/create?email=${userState.user.email}`,{
                    method: 'POST',
                    headers: new Headers({'Authorization' : userState.user.token})
            })
            .then(response => response.json())
            .then(json => {
                
                if(!json.Error){
                    console.log('Successfully Checkout created!!');
                    const userState = JSON.parse(sessionStorage.getItem('userState'));
                    userState.purchase_id = json.PurchaseOrderId;
                    let itemsStr = "";
                    userState.cart.map((item, index)=>{
                        itemsStr+=`${item.id}-${item.price}`
                        if(index < userState.cart.length-1){
                            itemsStr+=",";
                        }
                    });
                    fetch(`${URL}/order/addItems?PurchaseOrderId=${userState.purchase_id}&&items=${itemsStr}`, {
                        method: 'POST',
                        headers: new Headers({'Authorization' : userState.user.token})
                    }).then(response => response.json())
                    .then(json => {
                        if(json.Result){
                            console.log("Successfully added cart items");
                            console.log("Before checkout moving")
                            navigate('/browse/product/checkout');
                            console.log("After checkout")
                        }
                    }).catch((error) => console.log(error));
                    
                }else{
                    console.log('Failed');
                };
            }).catch((error)=> console.log(error));


            
        }else{
            console.log('/browse/product/checkout')
            sessionStorage.setItem('userState', JSON.stringify(userState));
            navigate('/sign-in', {state:{authenticated: false}});
        }
      }
  }



  return (
    <div className='cart-container'>
        <div className='cart-wrapper'>
            <h1>Shopping Cart</h1>
            <div className='top-wrapper'>
               
                <button className='cont-shopping-btn'>CONTINUE SHOPPING</button>
                <button className='checkout-btn' onClick={()=>readyForCheckout()}>CHECKOUT</button>
                
            </div>
            
            {JSON.parse(sessionStorage.getItem('userState')).cart.length>0 && (
            <div className='bottom-wrapper'>
                <div className='summary-container'>
                    <h1>ORDER SUMMARY</h1>
                    <div className='summary-item'>
                        <div className='summary-item-text'>
                            <span>Taxes</span>
                        </div>
                        <div className='summary-item-price'>
                            <span>${taxes}</span>
                        </div>
                    </div>
                    <div className='summary-item'>
                        <div className='summary-item-text'>
                            <span>Subtotal</span>
                        </div>
                        <div className='summary-item-price'>
                            <span>${subtotal}</span>
                        </div>
                    </div>
                    <div className='summary-item'>
                        <div className='summary-item-text'>
                            <span><b>Total</b></span>
                        </div>
                        <div className='summary-item-price'>
                            <span>${total}</span>
                        </div>
                    </div>
                </div>
            </div>)}
            {displayCartProducts}
        </div>
    </div>
  )

  
}

export default Cart
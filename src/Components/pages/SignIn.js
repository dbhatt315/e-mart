import React, {useState, useContext} from 'react';
import { Button } from '../Button';
import './SignIn.css';
import {Link, useNavigate} from 'react-router-dom';
import { SummaryContext } from '../Context';
const SignIn = () => {
    const [email,setEmail] = useState(null)
    const [passowrd,setPassword] = useState(null)
    const switchButton = () => setButtonState(!userLoginLogoutBtnState);
    const {userLoginLogoutBtnState, setButtonState} = useContext(SummaryContext);
    const [emailInput, setEmailInput] = useState("");
    const [passwordInput, setPasswordInput] = useState("");
    const [errorThrown, setErrorThrown] = useState(false);
    const errorThrownToggle = () => setErrorThrown(!errorThrown);
    const navigate = useNavigate();
    var userState = JSON.parse(sessionStorage.getItem('userState'));
    const URL = 'http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com'
  return (
    <div className='signIn_container'>
        <div className='signIn_wrapper'>
            <h1>Sign In</h1>
            <form>
                <p className='error-thrown' style={{ display: !errorThrown ? "none" : "block"}}>*Incorrect email or password</p>
                <input placeholder='Email' onChange={(e)=>setEmailInput(e.target.value)} required/>
                <input placeholder='Password' onChange={(e)=>setPasswordInput(e.target.value)} type='password' required/>
                <div className='signIn-btns'>
                {/* <Link to='/browse'> */}
                    <Button className='btns' buttonStyle='btn--outline'
                    buttonSize='btn--large' type='button' onClick={()=>{
                         fetch(`http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com/user/login?email=${emailInput}&password=${passwordInput}`, {
                            method: 'GET',
                        })
                        .then(response => response.json())
                        .then(json => {
                            if(!json.error){
                                
                                userState.user.email = json.email;
                                userState.user.token = json.token;
                                userState.user.tokenDate = json.date;
                                userState.user.authenticated = true;
                                userState.user.role = "CUSTOMER";
                                sessionStorage.setItem('userState', JSON.stringify(userState))
                                navigate('/browse');
                                switchButton();
                                setErrorThrown(false);
                            }else{
                                if (!errorThrown){
                                    setErrorThrown(true);
                                }
                            };
                        }).catch((error)=> console.log(error));
                    }}>Sign In</Button>
                {/* </Link> */}
                </div>
                <div>
                    <Link to='/sign-up'>
                        <span className='sign-up-link'cursor='pointer'>Create a new account</span>
                    </Link>
                </div>
            </form>
        </div>
    </div>
  )
}
 
export default SignIn
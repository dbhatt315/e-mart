import React from 'react'
import CartProduct from './CartProduct'


const CartProducts = ({items}) => {
  console.log("SOMETHOG");
  return (
    <div className='cart-products-container'>
        {items.map((item) => (
            <CartProduct item={item} key={(item.id)*(Math.floor(Math.random() * 1000) + 1)}/>
        ))}
    </div>
  )
}

export default CartProducts
import React, {useState, useEffect} from 'react'
import './Catalog.css';
import Products from '../Products';


const Catalog = () => {
    const [toptionsName, setTOptionsName] = useState([]);
    const [boptionsName, setBOptionsName] = useState([]);
    const [products, setProducts] = useState({});
    const [isEmpty, setIsEmpty] = useState(true);
    const URL = 'http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com';

    useEffect(() => {
        fetch(`${URL}/catalog/allItems`)
        .then(response => response.json())
        .then((json) =>{
            setProducts(json.items);
            setIsEmpty(false);
        }).catch((error)=>{
            setProducts({});
            setIsEmpty(true);
            console.log("Error: ", error);
        });
        
        fetch(`${URL}/catalog/allBrands`)
        .then(response => response.json())
        .then((json) =>{
            setBOptionsName(Object.values(json.items));
        }).catch((error)=>{
            setBOptionsName([]);
            console.log("Error: ", error);
        });


        fetch(`${URL}/catalog/allTypes`)
        .then(response => response.json())
        .then((json) =>{
            setTOptionsName(Object.values(json.items));
        }).catch((error)=>{
            setTOptionsName([]);
            console.log("Error: ", error);
        });

    }, []);

    useEffect(()=>{
    }, [products]);

    const handleBrandTypeChange = (e) => {
        const isBrand = e.target.value.substring(e.target.value.lastIndexOf("-brand"))==="-brand";
        if(e.target.value==="All"){
            fetch(`${URL}/catalog/allItems`)
            .then(response => response.json())
            .then((json) =>{
                setProducts(json.items);
                setIsEmpty(false);
            }).catch((error)=>{
                setProducts({});
                setIsEmpty(true);
                console.log("Error: ", error);
            });
        }else if(isBrand){
            const value = e.target.value.substring(0,e.target.value.lastIndexOf("-brand"))
            fetch(`${URL}/catalog/itemsByBrand?brand=${value}`)
            .then(response => response.json())
            .then((json) =>{
                setProducts(json.items);
                setIsEmpty(false);
            }).catch((error)=>{
                setProducts({});
                setIsEmpty(true);
                console.log("Error: ", error);
            });

        }else{
            const value = e.target.value.substring(0,e.target.value.lastIndexOf("-type"))
            fetch(`${URL}/catalog/itemsByType?type=${value}`)
            .then(response => response.json())
            .then((json) =>{
                setProducts(json.items);
                setIsEmpty(false);
            }).catch((error)=>{
                setProducts({});
                setIsEmpty(true);
                console.log("Error: ", error);
            });
        }
        console.log(e.target.value);
    }
    

    

    



  return (
    <div>
        <div className='filter-container'>
            <div className='filter-left'>
                <span className='filter-products-text'>Filter products:</span>
                <select className='select' onChange={(e) => handleBrandTypeChange(e)}>
                    <option>All</option>
                    <option disabled>
                        By Brand
                    </option>
                    {boptionsName.map((item)=>(
                        <option value={item.concat("-brand")}>{item}</option>
                    ))}
                {/* </select> */}
                {/* <select className='select' onChange={(e) => handleTypeChange(e)}> */}
                    <option disabled>
                        By Type
                    </option>
                    {toptionsName.map((item)=>(
                        <option value={item.concat("-type")}>{item}</option>
                    ))}
                </select>
            </div>
            <div className='filter-right'>
                {/* <div className='search-container'>
                    <input className='search-input' placeholder='Search...'></input>
                    <Search style={{ color: "gray", fontSize: 16 }} />
                </div> */}
            </div>
        </div>
        {Object.values.length  && <Products items={products}/>}
        {isEmpty && 
        <div className='error-container'>
            <h1>No items found!</h1>
        </div>}
    </div>
  )
}

export default Catalog
import React, {useContext, useState} from 'react'
import { SummaryContext } from '../Context';
import './CartProduct.css';
import CartProducts from './CartProducts';


const CartProduct = ({item}) => {
  const [qtnCount, setQtnCount] = useState(item.qtn);
  const {subtotal, setSubTotal, total, setTotal,changeInCart,setChangeInCart, displayCartProducts, setDisplayCartProducts} = useContext(SummaryContext);

  var userState = JSON.parse(sessionStorage.getItem('userState'));
  return (
    <div className='singleProduct-container'>
        <div className='productImage-container'>
            <img src={item.link}/>
            <div className='productDetail'> 
                <span className='productName'><b>{item.name}</b></span>
                <span className='productPrice'><b>${item.price}</b></span>
                <div className='cart-add-container'>
                        <div className='cart-amount-container'>
                            <i className="fa-solid fa-minus" onClick={()=>{
                              userState = JSON.parse(sessionStorage.getItem('userState'));
                              var qtn=qtnCount;
                              if (qtn>1){
                                qtn -= 1;
                                const found_elem = userState.cart.find(elem => elem.id==item.id);
                                if (found_elem){
                                  found_elem.qtn=qtn;
                                }
                                
                              }
                              userState.user_cart.subtotal=0;
                              for(const item of userState.cart){
                                userState.user_cart.subtotal += Math.round(((item.price*qtn) + Number.EPSILON) * 100) / 100;
                              }
                              setSubTotal(userState.user_cart.subtotal);
                              sessionStorage.setItem('userState', JSON.stringify(userState))
                              setQtnCount(qtn);
                            }}></i>    
                            <span className='cart-amount'>{qtnCount}</span>
                            <i className='fa-solid fa-plus' onClick={()=>{
                              userState = JSON.parse(sessionStorage.getItem('userState'));
                              var qtn=qtnCount
                              qtn += 1;                             
                              const ind = userState.cart.findIndex((elem)=>elem.id==item.id);    
                              userState.cart[ind].qtn=qtn;
                              userState.user_cart.subtotal=0;
                              for(const singleItem of userState.cart){
                                userState.user_cart.subtotal += Math.round(((singleItem.price*qtn) + Number.EPSILON) * 100) / 100;
                              }
                              setSubTotal(userState.user_cart.subtotal);
                              sessionStorage.setItem('userState', JSON.stringify(userState))
                              setQtnCount(qtn);
                            }}/>
                        </div>
                        <button className='cart-delete-btn' onClick={()=>{
                          for(let i =0;i<userState.cart.length;i++){
                            if(userState.cart[i].id==item.id){
                                userState.cart.splice(i,1);
                            }
                          }
                          userState.user_cart.subtotal=0;
                          for(const singleItem of userState.cart){
                            userState.user_cart.subtotal += Math.round(((singleItem.price*singleItem.qtn) + Number.EPSILON) * 100) / 100;
                          }
                          setSubTotal(userState.user_cart.subtotal);
                          sessionStorage.setItem('userState', JSON.stringify(userState))
                          setDisplayCartProducts(<CartProducts items={userState.cart}/>)
                        }}>Delete</button>    
                </div>
            </div>
        </div>
        <div className='line'></div>
     </div>
  )
}

export default CartProduct
import React, {useState} from 'react'
import './Analytics.css'

const Analytics = () => {
    const [toggle, setToggle] = useState(1);
    const [websiteUsage, setWebsiteUsage] = useState([]);
  return (
    <div className="App">
        <div className='analytics-filter-container'>
            <div className='analytics-filter-left'>
                <span className='analytics-filter-products-text'>Choose Report:</span>
                <select className='analytics-select'>
                    <option defaultValue='true'>
                        Website Usage
                    </option>
                    <option>
                        Items Sold
                    </option>
                </select>
            </div>
        </div>
        <div className='analytics-website-usage-container'>
            {
                //website usage
                toggle && toggle == 1 && 
                (<table className='analytics-website-usage'>
                    <tr>
                        <th>BID</th>
                        <th>Ip Address</th>
                        <th>Day</th>
                        <th>Event Type</th>
                    </tr>
                    {websiteUsage && websiteUsage.map((val, key)=>{
                        <tr key={key}>
                            <td>{val.bid}</td>
                            <td>{val.ipaddress}</td>
                            <td>{val.day}</td>
                            <td>{val.eventtype}</td>
                        </tr>
                    })}
                </table>)
            }
        </div>
        {
            //items sold
            toggle && toggle == 2 && 
            (<table className='analytics-items-sold'>
                <tr>
                    <th>BID</th>
                    <th>Ip Address</th>
                    <th>Day</th>
                    <th>Event Type</th>
                </tr>
                {websiteUsage.map((val, key)=>{
                    <tr key={key}>
                        <td>{val.bid}</td>
                        <td>{val.ipaddress}</td>
                        <td>{val.day}</td>
                        <td>{val.eventtype}</td>
                    </tr>
                })}
            </table>)
        }


      {/* <table>
        <tr>
          <th>Name</th>
          <th>Age</th>
          <th>Gender</th>
        </tr>
        {data.map((val, key) => {
          return (
            <tr key={key}>
              <td>{val.name}</td>
              <td>{val.age}</td>
              <td>{val.gender}</td>
            </tr>
          )
        })}
      </table> */}
    </div>
  )
}

export default Analytics
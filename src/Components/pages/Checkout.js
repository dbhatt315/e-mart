import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import {Button} from '../Button'
import './Checkout.css'

const Checkout = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [addressBill,setAddressBill] = useState("");
    const [provinceBill,setProvinceBill] = useState("");
    const [countryBill,setCountryBill] = useState("");
    const [zipBill,setZipBill] = useState("");
    const [addressShip,setAddressShip] = useState("");
    const [provinceShip,setProvinceShip] = useState("");
    const [countryShip,setCountryShip] = useState("");
    const [zipShip,setZipShip] = useState("");

    const URL = 'http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com';
    const navigate = useNavigate();
  useEffect(()=>{
      const userState = JSON.parse(sessionStorage.getItem('userState'));
      console.log("AUTH?", userState);
        if(userState.user.authenticated == true){
            console.log("AUTH1?", userState);

            fetch(`${URL}/user/getAddress?email=${userState.user.email}`,{
                    method: 'GET',
                    headers: new Headers({'Authorization' : userState.user.token})
            })
            .then(response => response.json())
            .then(json => {
                console.log("Response: ",json);
                if(json.User.firstname){
                    userState.firstName = json.User.firstname;
                }
                if(json.User.lastname){
                    userState.lastName = json.User.lastname;
                }
                if(json.Billing.street){
                    userState.addressBill = json.Billing.street.replaceAll("___", " ");
                }
                if(json.Billing.province){
                    userState.provinceBill = json.Billing.province;
                }
                if(json.Billing.country){
                    userState.countryBill = json.Billing.country;
                }
                if(json.Billing.zip){
                    userState.zipBill = json.Billing.zip;
                }
                if(json.Shipping.street){
                    userState.addressShip = json.Shipping.street.replaceAll("___", " ");
                }
                if(json.Shipping.province){
                    userState.provinceShip = json.Shipping.province;
                }
                if(json.Shipping.country){
                    userState.countryShip = json.Shipping.country;
                }
                if(json.Shipping.zip){
                    userState.zipShip = json.Shipping.zip;
                }
                setFirstName(userState.firstName);
                setLastName(userState.lastName);
                setAddressBill(userState.addressBill);
                setProvinceBill(userState.provinceBill);
                setCountryBill(userState.countryBill);
                setZipBill(userState.zipBill);
                setAddressShip(userState.addressShip);
                setProvinceShip(userState.provinceShip);
                setCountryShip(userState.countryShip);
                setZipShip(userState.zipShip);
            }).catch((error)=> {
            alert('Invalid authorization!');
            console.log(error)});

            

        // //     sessionStorage.setItem('userState', JSON.stringify(userState));
        }
  }, []);

  const placeOrder = () => {
    const userState = JSON.parse(sessionStorage.getItem('userState'));
    fetch(`${URL}/order/process?PurchaseOrderId=${userState.purchase_id}`, {
        method: 'POST',
        headers: new Headers({'Authorization' : userState.user.token})

    }).then(response => response.json())
    .then(json => {
        if(json["Order Status"] == "Processed"){
            userState = JSON.parse(sessionStorage.getItem('userState'));

            userState.cart.map((item, index)=>{
                fetch(`https://geolocation-db.com/json/`, {
                    method: 'POST',
                })
                .then(response => response.json())
                .then(json => {
                    const ip = json.IPv4;
                    const d = new Date()
                    const day = d.getFullYear() * 10000 + (d.getMonth() + 1) * 100 + d.getDate()

                    fetch(`http://emart-env.eba-prgeqgpp.us-east-1.elasticbeanstalk.com/analytics/addappevent?ip=${ip}&bid=${item.id}&day=${day}&event=PURCHASE`, { method: 'POST', })
                });
            });
            userState.cart = [];
            console.log("Successfully purchased");
            alert("Successfully purchased");
            navigate("/browse");
        } else {
            alert("Order declined");
        }
    }).catch((error) => console.log(error));
  }

  return (
    <div className='checkout-container'>
        <div className='checkout-wrapper'>
            <h1>Checkout</h1>
            <form>
                <h3>Billing Information</h3>
                <div className='name-container'>
                    <input placeholder='First Name' required value={firstName}></input>
                    <input placeholder='Last Name' required value={lastName}></input>
                </div>
                <input placeholder='Address' required value={addressBill}></input>
                <input placeholder='Province' required value={provinceBill}></input>
                <div className='countryzipcode-container'>
                    <input placeholder='Country' required value={countryBill}></input>
                    <input placeholder='Zip Code' required value={zipBill}></input>
                </div>
                <h3>Shipping Information</h3>
                <div className='name-container'>
                    <input placeholder='First Name' required value={firstName}></input>
                    <input placeholder='Last Name'required value={lastName}></input>
                </div>
                <input placeholder='Address' required value={addressShip}></input>
                <input placeholder='Province' required value={provinceShip}></input>
                <div className='countryzipcode-container'>
                    <input placeholder='Country' required value={countryShip}></input>
                    <input placeholder='Zip Code' required value={zipShip}></input>
                </div>
                <h3>Payment Information</h3>
                <input placeholder='Name on card' required></input>
                <div className='cardnumbercvv-container'>
                    <input placeholder='Card Number' required></input>
                    <input placeholder='CVV' required></input>
                </div>
                <div className='btns-container-checkout'>
                    <Button className='btns' buttonStyle='btn--outline'
                        buttonSize='btn--large' type="button" onClick={() => placeOrder()}>Confirm Order</Button>
                </div>
            </form>
        </div>
    </div>
  )
}

export default Checkout
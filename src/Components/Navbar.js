import React, {useState, useContext} from 'react'
import {Link} from 'react-router-dom';
import './Navbar.css';
import {Button} from './Button';
import { SummaryContext } from './Context';
function Navbar() {
  
  const [click, setClick] = useState(false);
  const switchButton = () => setButtonState(!userLoginLogoutBtnState);
  const closeMobileMenu = () => setClick(false);
  const handleClick = () => setClick(!click);
  const {userLoginLogoutBtnState, setButtonState} = useContext(SummaryContext);

  return (
    <>
    <nav className="navbar">
        <div className="navbar-container">
            <Link to="/" className="navbar-logo">
            E-Mart <i className='fa-solid fa-shop' />
            </Link>
            <div className="menu-icon" onClick={handleClick}>
              <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
            </div>
      
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                Home
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/browse' className='nav-links' onClick={closeMobileMenu}>
                Browse
              </Link>
            </li>
            
            {!userLoginLogoutBtnState && <li className='nav-item'>
              <Link to='/sign-in' className='nav-links'>
                Sign In
              </Link>
            </li>}
          </ul>
          {userLoginLogoutBtnState ? <Link to='/'>
            <Button buttonStyle='btn--outline' onClick={()=>{
              var userState = JSON.parse(window.sessionStorage.getItem('userState'));
              switchButton();
              userState.cart = [];
              userState.user.email= '';
              userState.user.token= null;
              userState.user.tokenDate= null;
              userState.user.role= null;
              userState.user.authenticated= false;
              window.sessionStorage.setItem('userState', JSON.stringify(userState));
            }
          }>LOG OUT</Button></Link> : <Link to='/sign-up'><Button buttonStyle='btn--outline'>SIGN UP</Button></Link>}
          
          <Link to='/shopping_cart' className='cart-logo' onClick={closeMobileMenu}>
            <i className="fa-solid fa-cart-shopping"/>
          </Link>
        
        </div>
    </nav>
    </>
  )
}

export default Navbar
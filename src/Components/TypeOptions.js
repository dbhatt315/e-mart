import React, { useState } from 'react'

const TypeOptions = () => {
    const [toptionsName, setTOptionsName] = useState([]);
    setTOptionsName(["Watches", "Clothes"]);
    
    return (
        <div>
            {toptionsName && toptionsName.map((optionName) =>(
                <option>{optionName}</option>
            ))}
        </div>
    )
}

export default TypeOptions;
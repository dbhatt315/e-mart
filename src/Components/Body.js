import React from 'react'
import { Button } from './Button'
import './Body.css';
import {Link} from 'react-router-dom';
function Body() {
  return (
    <div className='body-container'>
        <video src="/videos/body_background.mp4" autoPlay loop muted />
        <h1>SHOP NOW</h1>
        <p>What are you waiting for?</p>
        <div className='body-btns'>
        <Link to='/sign-up'>
            <Button className='btns' buttonStyle='btn--outline'
            buttonSize='btn--large'>GET STARTED</Button>
        </Link>
        </div>
    </div>
  )
}

export default Body
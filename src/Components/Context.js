import { useState, createContext, useEffect } from "react";

const SummaryContext = createContext();

const ContextProvider = ({children}) => {
    const [subtotal, setSubTotal] = useState(0.0);
    const [total, setTotal] = useState(0.0);
    const [taxes, setTaxes] = useState(0.0);
    const [changeInCart, setChangeInCart] = useState(1);
    const [displayCartProducts, setDisplayCartProducts] = useState([]);
    const [userLoginLogoutBtnState, setButtonState] = useState(false);
    useEffect(() => {
        setTotal(Math.round(((subtotal*1.15) + Number.EPSILON) * 100) / 100);
    }, [subtotal]);

    useEffect(()=>{
        setTaxes(Math.round(((subtotal*0.15) + Number.EPSILON) * 100) / 100);
    },[subtotal]);



    return (
        <SummaryContext.Provider value={{
            subtotal,
            setSubTotal,
            total,
            setTotal,
            taxes,
            setTaxes,
            changeInCart,
            setChangeInCart,
            displayCartProducts,
            setDisplayCartProducts,
            userLoginLogoutBtnState, 
            setButtonState
        }}>
            {children}
        </SummaryContext.Provider>

    );
};

export {ContextProvider, SummaryContext};
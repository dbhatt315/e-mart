import React from 'react'
import './Products.css'
import Product from './Product'

const Products = ({items}) => {
    Object.keys(items).map(function(key, index) {
        items[key].id = key;
        items[key].name = items[key].name.replaceAll('___', " ");
        items[key].description = items[key].description.replaceAll('___', " ");
    })
  return (
    <div className='products-container'>
        {Object.values(items).map((item) =>(
            <Product item={item} key={item.id}/>
        ))}
    </div>
  )
}

export default Products
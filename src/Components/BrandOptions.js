import React, { useState } from 'react'

const BrandOptions = () => {
    const [boptionsName, setBOptionsName] = useState([]);

    setBOptionsName(["Apple", "Samsung"]);
  return (
    <div>
        {boptionsName.map((optionName) =>(
            <option>{optionName}</option>
        ))}
    </div>
  )
}


export default BrandOptions; 

import React from 'react'
import './Product.css'
import {useNavigate} from 'react-router-dom';

const Product = ({item}, {key}) => {
  const navigate = useNavigate()
  return (
    
      <div className='product-container' onClick={()=>{navigate('/browse/product',{state:item})}}>
          <img className='product-img' src={item.link}/>
          <h2 className='product-name'>{item.name}</h2>
      </div>
  )
}

export default Product